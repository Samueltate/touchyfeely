﻿ using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

// struct = class without function
[System.Serializable]
public struct Gesture
{
    public string name;
    public List<Vector3> fingerDatas;
    public UnityEvent onRecognized;
}

public class GestureDetector : MonoBehaviour
{
    // Start is called before the first frame update

    public float threshold = 0.1f;
    public OVRSkeleton skeleton;
    public bool debugMode = true;
    public List<Gesture> gestures;
    public List<OVRBone> fingerBones;
    private Gesture previousGesture;
    public UnityEvent cancelGesture;
    public Gesture nullGesture;
    public OVRHand hand;

    void Start()
    {
        Invoke("GetFingerBones", 3);
        previousGesture = new Gesture();
    }

    void GetFingerBones()
    {
        fingerBones = new List<OVRBone>(skeleton.Bones);

    }

    // Update is called once per frame
    void Update()
    {
        if(fingerBones == null) fingerBones = new List<OVRBone>(skeleton.Bones);
       
        if (debugMode && Input.GetKeyDown(KeyCode.Space)) 
        {
            Save(); 
        }

        Gesture currentGesture = Recognise();

        bool hasRecognized = !currentGesture.Equals(new Gesture());

        if(hasRecognized && !currentGesture.Equals(previousGesture))
        {
            Debug.Log("New Gesture Found : " + currentGesture.name);
            previousGesture = currentGesture;
            currentGesture.onRecognized.Invoke();
        }

        if (hand.HandConfidence == OVRHand.TrackingConfidence.Low)
        {
            cancelGesture.Invoke();
        };
    }

    void Save()
    {
        fingerBones = new List<OVRBone>(skeleton.Bones);

        Gesture g = new Gesture();
        g.name = "New Gesture";
        List<Vector3> data = new List<Vector3>();

        foreach (var bone in fingerBones)
        {
            // finger position relative to root
            data.Add(skeleton.transform.InverseTransformPoint(bone.Transform.position));
            Debug.Log("found bone" + bone.Id);
        }

        g.fingerDatas = data;
        gestures.Add(g); 
    }

    Gesture Recognise()
    {
        Gesture currentGesture = new Gesture();
        float currentMin = Mathf.Infinity;
        bool gestureFound = false;
        foreach (var gesture in gestures)
        {
            float sumDistance = 0;
            bool isDiscarded = false;
            for (int i = 0; i < fingerBones.Count; i++) 
            {
                Vector3 currentData = skeleton.transform.InverseTransformPoint(fingerBones[i].Transform.position);
                float distance = Vector3.Distance(currentData, gesture.fingerDatas[i]);
                if(distance > threshold)
                {
                    isDiscarded = true;
                    //if (gesture.name == currentGesture.name) {
                    //    coolDownFrames++;
                    //    if(coolDownFrames > 20)
                    //    {
                    //        cancelGesture.Invoke();
                    //        coolDownFrames = 0;
                    //    }
                    //} else
                    //{
                    //    coolDownFrames = 0;
                    //}
                    break;
                }
                
                sumDistance += distance;

            }
            if(!isDiscarded && sumDistance < currentMin)
            {
                currentMin = sumDistance;
                currentGesture = gesture;
                gestureFound = true;
            }

        }
        //Debug.Log("gesture received right now : " + currentGesture.name);
        if (!gestureFound) return nullGesture;
        else return currentGesture;
    }

    bool cooldown;
    int coolDownFrames;

    public void Cooldown()
    {
        cooldown = false;
    }
}
