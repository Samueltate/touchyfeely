﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class GrabTracker : MonoBehaviour
{
    public List<GrabInfo> grabsStarted = new List<GrabInfo>();
    public bool grabbing;

    public Rigidbody rb;

    // Update is called once per frame
    void Update()
    {
        if (grabbing) {
            GetVelocity();
        }
    }

    public int velocityFrames = 10;
    public Vector3 lastFramePosition;
    public List<Vector3> differences = new List<Vector3>();
    public Vector3 smoothedVelocity;

    public void GetVelocity()
    {
        differences.Add(transform.position - lastFramePosition);
        if(differences.Count > velocityFrames)
        {
            differences.RemoveAt(0);
        }

        foreach(Vector3 difference in differences)
        {
            smoothedVelocity += difference;
        }

        smoothedVelocity = smoothedVelocity / differences.Count;
        lastFramePosition = transform.position;

    }

    public void UpdateGrabFinger(Grabable grabCandidate, bool contactBegun, bool thumb)
    {
        int grabID = 0;
        if (contactBegun)
        {
            grabbing = true;
            bool grabExists = false;
            //Debug.Log("Contact received");
            foreach(GrabInfo grab in grabsStarted)
            {
                if (grab.grabCandidate == grabCandidate)
                {
                    if (thumb)
                    {
                        grab.thumb = true;
                    }
                    else
                    {
                        grab.fingers++;
                    }
                    //Debug.Log("Found existing grab");

                    grabExists = true;
                    CheckGrab(grab, grabID);
                    return;
                }

                grabID++;
            }

            //checks if any grab started
            if(!grabExists)
            {
                grabExists = true;
                //Debug.Log("No grab found");

                //starts a new grab and adds to array
                GrabInfo newGrab = new GrabInfo();
                newGrab.grabCandidate = grabCandidate;

                //increments contact points
                if (thumb)
                {
                    newGrab.thumb = true;
                }
                else
                {
                    newGrab.fingers++;
                }

                grabsStarted.Add(newGrab);
            }
        }
        else
        {

            foreach (GrabInfo grab in grabsStarted)
            {
                if (grab.grabCandidate == grabCandidate)
                {
                    if (thumb)
                    {
                        grab.thumb = false;
                    }
                    else
                    {
                        grab.fingers--;
                    }

                    CheckGrab(grab, grabID);
                    return;

                }

                grabID++;
            }
        }


    }

    public void CheckGrab(GrabInfo grabToCheck, int grabID)
    {
        //Debug.Log(grabToCheck);
        //Debug.Log("Checking grab, grab candidate is " + grabToCheck.grabCandidate.name +
        //   " finger count is " + grabToCheck.fingers +
        //   "thumb value is " + grabToCheck.thumb);

        if (!grabsStarted[grabID].grabbed && grabsStarted[grabID].fingers > 0 && grabsStarted[grabID].thumb)
        {
            
            grabsStarted[grabID].grabbed = true;
            //Debug.Log("Grab Begun");

            grabsStarted[grabID].grabCandidate.transform.SetParent(transform,true);
            grabsStarted[grabID].grabCandidate.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        }
        else if (grabsStarted[grabID].grabbed && (grabsStarted[grabID].fingers < 1 || !grabsStarted[grabID].thumb))
        {
            grabsStarted[grabID].grabbed = false;
            grabbing = false;

            grabsStarted[grabID].grabCandidate.transform.parent = null;
            grabsStarted[grabID].grabCandidate.gameObject.GetComponent<Rigidbody>().isKinematic = false;

            grabsStarted[grabID].grabCandidate.gameObject.GetComponent<Rigidbody>().AddForce(smoothedVelocity*10000);
            grabsStarted.RemoveAt(grabID);
            //Debug.Log("Grab Ended");

        }

        else if (grabsStarted[grabID].fingers < 1 && !grabsStarted[grabID].thumb)
        {
            //Will need a way of tracking index of grab anbd clearing that but later
            grabsStarted.RemoveAt(grabID);
            //Debug.Log("Grab Ended");
        }
    }

}
[System.Serializable]
public class GrabInfo
{
    public Grabable grabCandidate;
    public int fingers;
    public bool thumb;
    public bool grabbed;
    public int framesReleased;

}