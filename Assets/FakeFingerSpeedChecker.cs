﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeFingerSpeedChecker : MonoBehaviour
{
    public Transform joint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Vector3 currentPosition;
    public Vector3 lastPosition;
    public float bufferedSpeed;
    public List<float> speedList = new List<float>();
    public int smoothing = 20;

    // Update is called once per frame
    void Update()
    {
        currentPosition = transform.position - joint.position;
        float newSpeed = (currentPosition - lastPosition).magnitude * 10;

        if (speedList.Count < smoothing)
        {
            speedList.Add(newSpeed);

        } else
        {
            speedList.RemoveAt(0);
            speedList.Add(newSpeed);
        }

        foreach(float speed in speedList)
        {
            bufferedSpeed += speed;
        }

        bufferedSpeed = (bufferedSpeed / speedList.Count);

        lastPosition = currentPosition;
    }


}
