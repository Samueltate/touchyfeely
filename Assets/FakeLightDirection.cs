﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeLightDirection : MonoBehaviour
{
    public Material rug;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rug.SetVector("_FakeDirection", transform.forward);
    }
}
