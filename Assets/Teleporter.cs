﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public bool teleporting;

    public Vector3 teleportTarget;

    public Transform playerSpace;
    public Transform playerHead;

    public LineRenderer line;

    public Transform handToFollow;

    public float rotationOffset;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LerpRotation();
        if (Input.GetKeyDown(KeyCode.A))
        {
            StartTeleport();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            CancelTeleport();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            ConfirmTeleport();
        }


        if (teleporting) { 
            RayCastTeleport();

        }

    }

    public void StartTeleport()
    {
        teleporting = true;
        CancelInvoke("CancelTeleport");
    }

    public void RayCastTeleport()
    {
        int layerMask = 1 << 8;
        layerMask = ~layerMask;
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            if (line.positionCount == 0)
            {
                line.positionCount = 2;
                line.SetPosition(0, transform.position);
                line.SetPosition(1, transform.position);

                //if (!rayActive)
                //{
                //    StartLerpToTarget();
                //}
            }


            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            //Debug.Log("Did Hit");
            teleportTarget = hit.point;
            //Debug.Log("teleportTarget is : " + teleportTarget);


            //if (rayActive)
            //{
                line.SetPosition(0, transform.position);
                line.SetPosition(1, Vector3.Slerp(line.GetPosition(1), teleportTarget, .2f));
                //get distance
                float distance = (transform.position - teleportTarget).magnitude;

            //}


            //line.material.SetTextureScale(1, new Vector2(distance, 1));


        }
        else
        {
            if (line.positionCount == 2) line.positionCount = 0;

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            rayActive = false;
            //Debug.Log("Did not hit");
        }
            
            //hit.distance, Color.yellow)
;    }

    public void ConfirmTeleport()
    {

        if (!teleporting) return;
        CancelTeleport();
        Vector3 player = playerHead.transform.localPosition;

        Vector3 flattenedPlayer = new Vector3(player.x, 0, player.z);

        playerSpace.position = teleportTarget - flattenedPlayer;


    }

    public void InvokeCancelTeleport()
    {
        Invoke("CancelTeleport", .3f);
    }

    public void CancelTeleport()
    {

        if (lerping)
        {
            StopCoroutine(lerpToTarget);
        }

        teleporting = false;
        rayActive = false;
        line.positionCount = 0;

    }

    public void LerpRotation()
    {
        this.transform.rotation = (Quaternion.Slerp(this.transform.rotation, handToFollow.rotation * Quaternion.Euler(rotationOffset, 0, 0), .2f));
        this.transform.position = Vector3.Slerp(this.transform.position, handToFollow.position, .2f);
    }

    //start of buffer smoothing for hand ray target
    public List<Quaternion> handAngles = new List<Quaternion>();
    public int angleBufferstrength;
    public void BufferHandTarget()

    {
        //if(handAngles.Count == 0)
        //{

        //}
        if (handAngles.Count < angleBufferstrength)
        {
            handAngles.Add(handToFollow.transform.rotation);
        }
        if(handAngles.Count <= angleBufferstrength)
        {
            handAngles.Add(handToFollow.transform.rotation);
            handAngles.RemoveAt(0);

        }

        Quaternion smoothAngle = new Quaternion();
            foreach(Quaternion quat in handAngles)
        {
            // thread on averaging quaternions https://forum.unity.com/threads/average-quaternions.86898/
        }


    }


    //Lerp that takes two target points and drives line renderer, so that it looks nice when it shoots out on active
    public IEnumerator lerpToTarget;
    bool lerping;
    bool rayActive;
    public Vector3 rayTarget;

    public void StartLerpToTarget()
    {
        if (lerping)
        {
            StopCoroutine(lerpToTarget);

        }

        lerpToTarget = LerpToTarget();
        StartCoroutine(lerpToTarget);

    }

    public IEnumerator LerpToTarget()
    {

        lerping = true;
        float i = 0;

        while(i < 1)
        {
            i += Time.deltaTime * 2;
            line.SetPosition(1, Vector3.Lerp(transform.position, teleportTarget, i));
            yield return null;
        }
        lerping = false;
        rayActive = true;
    }
}
