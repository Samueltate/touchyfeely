﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    public bool touching;
    public Transform sensorJoint;

    public float currentTemp;
    public float currentRough;

    public TrailRenderer touchTrail;

    public Material reactorShader;
    public SkinnedMeshRenderer meshToEffect;

    public IEnumerator newTouchInstance;
    public IEnumerator setAudioVolumeInstance;

    public string sensorID;

    public GameObject objectToAffect;

    public AudioSource audioSource1;
    public AudioSource audioSource2;
    public AudioClip roughClip;

    public TrailObject currentTrail;

    //brushing audio variables
    public Vector3 currentPositionRelativeToJoint;
    public Vector3 lastPosition;
    public float bufferedSpeed;
    public List<float> speedList = new List<float>();

    public TouchProperties currentlyTouched;

    public AudioSource voAudio;

    public static List<string> audioClipsPlayed = new List<string>();

    #region COLLISION LOGIC
    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("OnColissionEnter");

        if (collision.gameObject.GetComponent<TouchProperties>() && endSoon)
        {
            endSoon = false;
            CancelInvoke("EndTouchAfterFrame");
        }

        if (collision.gameObject.GetComponent<TouchProperties>() && !touching)
        {
            //Debug.Log("newTouch");

            if (endTouchLerping)
            {
                StopCoroutine(endTouchInstance);
                endTouchLerping = false;
            }


            //Debug.Log("Kicking off color touch");
            touching = true;
            TouchProperties touch = collision.gameObject.GetComponent<TouchProperties>();
            currentlyTouched = touch;
            //gets ready to pass temperature (as value) and contact point to shader
            targetTemp = touch.temperature;
            targetPos = objectToAffect.transform.InverseTransformPoint(collision.GetContact(0).point);
            //ApplyColourOnTouch();
            meshToEffect.material.SetVector(sensorID + "Position", targetPos);
            meshToEffect.materials[0].SetFloat(sensorID + "Temperature", targetTemp);
            SetPressure();
            CheckSpeedOfFinger();
            //sets up speed buffer that controls brushing audio
            //audioSource1.volume = 0;
            HandleAudioClip();
            //audioRough.pitch = 0;
            bufferedSpeed = 0;
            speedList.Clear();

            if (collision.gameObject.GetComponent<TouchProperties>().voOnTouch != null) {

                string clipName = collision.gameObject.GetComponent<TouchProperties>().voOnTouch.name;

                //checks if that clip has been played, and plays/adds to played list if not
                foreach (string clip in audioClipsPlayed)
                {
                    if (clip == clipName) return;
                }
                Invoke("PlayVO", 1f);
                audioClipsPlayed.Add(clipName);

            }

            lastPosition = transform.position;

            //Requests trail and childs it and turns it on
            currentTrail = TrailHandler.instance.RequestTrail();

            if (currentTrail != null)
            {
                //currentTrail.Activate();
                currentTrail.transform.position = transform.position;
            }
            else
            {
                Debug.LogError("No trail received, your fingers aint dusty");
            }
 
        }

    }

    public void PlayVO() 
    {

        if (!voAudio.isPlaying && currentlyTouched.voOnTouch)
        {

            voAudio.clip = currentlyTouched.voOnTouch;
            voAudio.Play();
        }

    }

    public float startTemp;
    public float targetTemp;

    public float startPressure;
    public float currentPressure;

    public Vector3 startPos;
    public Vector3 currentPos;
    public Vector3 targetPos;

    bool newTouchLerping;

    public void ApplyColourOnTouch()
    {
        if (newTouchLerping)
        {

            StopCoroutine(newTouchInstance);
            newTouchLerping = false;

        }


        if (endTouchLerping)
        {
            StopCoroutine(endTouchInstance);
            endTouchLerping = false;
        }
        //meshToEffect.material.SetFloat(sensorID + "Strength", 0f);

        startTemp = currentTemp;
        //currentStrength = 0;
        //startStrength = currentStrength;
        startPos = targetPos;



        newTouchInstance = NewTouch();
        StartCoroutine(newTouchInstance);

    }

    public IEnumerator NewTouch()
    {

        newTouchLerping = true;
        float i = 0;
        while (i < 1)
        {
            i += Time.deltaTime / .5f;
            currentTemp = -Mathf.Lerp(startTemp, targetTemp, i);
            currentTemp += 1f;
            currentPos = Vector3.Lerp(startPos, targetPos, i);
            currentPressure = Mathf.Lerp(startPressure, 1, i);

            ///*instead of lerping to position, should set position and color & lerp strength then lerp strength down afterwards*/
            meshToEffect.material.SetVector(sensorID + "Position", currentPos);
            meshToEffect.materials[0].SetFloat(sensorID + "Temperature", targetTemp);
            //meshToEffect.materials[0].SetFloat(sensorID + "Strength", currentStrength);
            yield return null;

        }
        newTouchLerping = false;
        StopCoroutine(newTouchInstance);

    }


    public void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.GetComponent<TouchProperties>() && touching)
        {
            //Debug.Log("OnColissionStay");
            //currentTrail.SmoothFollow(transform.position);
            //audioSource1.volume = CheckDistanceFromTouch() / 2;
            //audioRough.pitch = CheckSpeedOfFinger() ;
            HandleAudioClip();
            //SetPressure();

                
        }
    }

    bool audioLooping;
    int strokeIndex;
    double audioSwapThreshold;
    AudioSource currentAudioSource;

    public void HandleAudioClip()
    {
        if (currentlyTouched.strokeSounds.Length < 1) return;
        if (!audioLooping)
        {
            //Debug.Log("AudioNot looping, lets get started");
            currentAudioSource = audioSource1;
            audioSource1.clip = currentlyTouched.strokeSounds[strokeIndex];
            audioSource1.Play();
            audioSwapThreshold = audioSource1.clip.length * .75;
            audioLooping = true;
            return;
        }
        //Debug.Log("time is " + currentAudioSource.time);
        //Debug.Log("threshold is " + audioSwapThreshold);

        if (currentAudioSource.time >= audioSwapThreshold)
        {
            //Debug.Log("we've hit our clip threshold");
            strokeIndex++;
            
            if(strokeIndex > currentAudioSource.clip.length-1)
            {
                //Debug.Log("Stroke index is " + strokeIndex + "We're setting stroke index to 0");
                strokeIndex = 0;
            }
            
            if(currentAudioSource = audioSource1)
            {
                currentAudioSource = audioSource2;
            } else
            {
                currentAudioSource = audioSource1;
            }

            currentAudioSource.clip = currentlyTouched.strokeSounds[strokeIndex];
            currentAudioSource.Play();
            audioSwapThreshold = currentAudioSource.clip.length * .75f;

        }


    }

    public void SetPressure()
    {
        currentPositionRelativeToJoint = transform.position - sensorJoint.transform.position;
        currentPressure = Mathf.SmoothDamp(currentPressure, currentPositionRelativeToJoint.magnitude * 100, ref smoothvelocity, 1f);

        meshToEffect.material.SetFloat(sensorID + "Strength", currentPositionRelativeToJoint.magnitude * 100);
    }

    #endregion

    #region BRUSHING AUDIO
    float smoothvelocity = 0.1f;

    public Vector3 currentPosition;
    public int smoothing = 3;

    public float CheckSpeedOfFinger()
    {

        currentPosition = sensorJoint.position - lastPosition;

        //Debug.Log(currentPosition);

        float newSpeed = currentPosition.magnitude * 20;
        //Debug.Log(newSpeed);
        if (newSpeed > 1.2) newSpeed = 1.2f;
        //Debug.Log(newSpeed);


        if (speedList.Count < smoothing)
        {
            speedList.Add(newSpeed);

        }
        else
        {
            speedList.RemoveAt(0);
            speedList.Add(newSpeed);
        }

        foreach (float speed in speedList)
        {
            bufferedSpeed += speed;
        }
         
        bufferedSpeed = (bufferedSpeed / speedList.Count);

        lastPosition = sensorJoint.position;
        return bufferedSpeed;
    }

    float distanceFromTouch;
    public float CheckDistanceFromTouch()
    {

        currentPositionRelativeToJoint = transform.position - sensorJoint.position;
        distanceFromTouch = (transform.position - sensorJoint.position).magnitude * 100;

        return distanceFromTouch;
    }
    #endregion

    #region TEMPERATURE PASSED AS COLOUR


    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<TouchProperties>())
        {
            Invoke("EndTouchAfterFrame", .1f);
            audioLooping = false;
            endSoon = true;
        }

        //Debug.Log("OnCollissionExit");

    }

    public void EndTouchAfterFrame()
    {

        //Debug.Log("touch end");

        touching = false;
        endSoon = false;


        HandleClearBufferOverFrames();
        HandlerEndTouch();

        //decouples current trail and turns it off
        if (currentTrail != null)
        {
            currentTrail.Deactivate();
            currentTrail = null;
        }
        else
        {
            //Debug.LogError("No current trail, so nothing to deactivate");
        }
    }
    public bool endSoon;

    //fills buffer up with 0 value, driving smoothed speed down, which controls volume 
    public void HandleClearBufferOverFrames()
    {
        if (clearingbuffer)
        {
            StopCoroutine(clearBufferOverFramesInstance);
        }

        clearBufferOverFramesInstance = ClearBufferOverFrames();
        StartCoroutine(clearBufferOverFramesInstance);


    }

    bool clearingbuffer;
    IEnumerator clearBufferOverFramesInstance;

    public IEnumerator ClearBufferOverFrames()
    {

        for (int i = 0; i < speedList.Count; i++)
        {
            speedList.RemoveAt(0);
            speedList.Add(0);

            //could this be its own method as used twice
            foreach (float speed in speedList)
            {
                bufferedSpeed += speed;
            }

            bufferedSpeed = (bufferedSpeed / speedList.Count);
            audioSource1.volume = bufferedSpeed / 2;
            audioSource1.pitch = bufferedSpeed;
            

            yield return null;
        }

        audioSource1.Stop();
        speedList.Clear();
        StopCoroutine(clearBufferOverFramesInstance);


    }

    bool endTouchLerping;

    public void HandlerEndTouch()
    {

        if (endTouchLerping)
        {
            StopCoroutine(endTouchInstance);
            endTouchLerping = false;
        }

        startPressure = currentPressure;

        endTouchInstance = EndTouch();
        StartCoroutine(endTouchInstance);

    }

    public IEnumerator endTouchInstance;

    public IEnumerator EndTouch()
    {
        endTouchLerping = true;
        float time = .1f;

        float i = 0;

        while (i < 1)
        {

            i += Time.deltaTime / time;

            currentPressure = Mathf.Lerp(startPressure, 0, i);
            meshToEffect.materials[0].SetFloat(sensorID + "Strength", currentPressure);

            //meshToEffect.materials[0].SetFloat(sensorID + "Strength", 1 - i);
            yield return null;

        }
        endTouchLerping = false;
        StopCoroutine(endTouchInstance);

    }

    #endregion

}
