﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailHandler : MonoBehaviour
{
    public int totalTrails;
    public TrailObject touchTrailPrefab;
    public List<TrailObject> touchTrails = new List<TrailObject>();

    public static TrailHandler instance;

    // Start is called before the first frame update
    void Awake()
    {

        if(instance == null)
        {
            instance = this;
        }
        
        for (int i = 0; i <totalTrails; i++)
        {
            TrailObject newTouchTrail = Instantiate(touchTrailPrefab);
            touchTrails.Add(newTouchTrail);
            newTouchTrail.gameObject.SetActive(false);
        }

    }

    public TrailObject RequestTrail()
    {
        foreach(TrailObject trail in touchTrails)
        {
            if (!trail.inUse)
            {
                trail.gameObject.GetComponent<TrailRenderer>().Clear();

                return trail;
            }
        }

        TrailObject newTouchTrail = Instantiate(touchTrailPrefab);
        newTouchTrail.gameObject.SetActive(false);
        touchTrails.Add(newTouchTrail);

        return newTouchTrail;
        
    }


}
