﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OculusSampleFramework;

public class SensorSpawner : MonoBehaviour
{

    public OVRSkeleton Skeleton;
    public Transform[] sensorJoints;
    public Sensor[] sensors;
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public Material materialInstance;

    // Start is called before the first frame update
    void Start()
    {
        Skeleton = GetComponent<OVRSkeleton>();
        //skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        materialInstance = skinnedMeshRenderer.material;
        StartCoroutine(WaitForSkeleton());

    }

    public IEnumerator WaitForSkeleton()
    {
        while (!Skeleton.IsInitialized )
        {
            yield return null;
        }

        AddSensorToTip();
    }

    public void AddRigidBody()
    {
        //Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_ThumbTip].position  = Vector3.zero ;
    }

    public void AddSensorToTip()
    {

        sensorJoints[0].SetParent(Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_IndexTip].Transform);
        sensorJoints[0].localPosition = Vector3.zero;
        //sensors[0].reactorShader = materialInstance;
        //sensors[0].meshToEffect = skinnedMeshRenderer;


        sensorJoints[1].SetParent(Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_MiddleTip].Transform);
        sensorJoints[1].localPosition = Vector3.zero;
        //sensors[1].reactorShader = materialInstance;
        //sensors[1].meshToEffect = skinnedMeshRenderer;


        sensorJoints[2].SetParent(Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_RingTip].Transform);
        sensorJoints[2].localPosition = Vector3.zero;
        //sensors[2].reactorShader = materialInstance;
        //sensors[2].meshToEffect = skinnedMeshRenderer;


        sensorJoints[3].SetParent(Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_PinkyTip].Transform);
        sensorJoints[3].localPosition = Vector3.zero;
        //sensors[3].reactorShader = materialInstance;
        //sensors[3].meshToEffect = skinnedMeshRenderer;


        sensorJoints[4].SetParent(Skeleton.Bones[(int)OVRPlugin.BoneId.Hand_ThumbTip].Transform);
        sensorJoints[4].localPosition = Vector3.zero;
        //sensors[4].reactorShader = materialInstance;
        //sensors[4].meshToEffect = skinnedMeshRenderer;



    }

    // Update is called once per frame
    void Update()
    {

    }
}
