﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchProperties : MonoBehaviour
{

    [Range(-100, 100)]
    public float temperature;


    [Range(-1, 1)]
    public float resistance;

    [Range (-1,1)]
    public float roughness;

    public AudioClip[] strokeSounds;

    public AudioClip voOnTouch;



}
