﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Trail renderer activate and deactivate handler
public class TrailObject : MonoBehaviour
{
    private Vector3 velocity = Vector3.zero;
    public bool inUse;
    Vector3 lastPos;
    public bool farEnough;

    public void Activate()
    {
        inUse = true;
        this.gameObject.SetActive(true);
        this.gameObject.GetComponent<TrailRenderer>().Clear();
        //transform.localPosition = Vector3.zero;

    }

    // TODO - Create fade out with end event for turn off
    public void Deactivate()
    {
        transform.parent = null;
        farEnough = false;
        Invoke("TurnOff", 0.2f);
    }

    public void TurnOff()
    {
        this.gameObject.GetComponent<TrailRenderer>().Clear();

        this.gameObject.SetActive(false);
        inUse = false;

    }

    public void SmoothFollow(Vector3 nextLocation)
    {
        float distance = (nextLocation - lastPos).magnitude;

        if (farEnough)
        {

            transform.position = Vector3.SmoothDamp(transform.position, nextLocation, ref velocity, .2f);
        }

        if(distance > .05f && farEnough == false)
        {
            transform.position = nextLocation;
            Activate();
            farEnough = true;
        }

        lastPos = nextLocation;

    }

}
