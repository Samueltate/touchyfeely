﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandReactor : MonoBehaviour
{
    public OVRMeshRenderer mesh;
    public SkinnedMeshRenderer renderer;
    public bool materialDisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mesh.IsDataHighConfidence && !materialDisplay)
        {
            renderer.material.SetFloat("_Confidence", 1f);
            materialDisplay = true;
        } 

        if(!mesh.IsDataHighConfidence && materialDisplay)
        {
            renderer.material.SetFloat("_Confidence", 0);
            materialDisplay = false;
        }


    }
}
