﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GrabPoints : MonoBehaviour
{
public GrabTracker grabTracker;
public bool thumb;
    private void OnTriggerEnter(Collider other)
    {
            //Debug.Log("Trigger enter " + other.gameObject.name);
        if (other.GetComponentInParent<Grabable>())
        {
            //Debug.Log("Grabable collided");
            grabTracker.UpdateGrabFinger(other.GetComponentInParent<Grabable>(), true, thumb);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.GetComponentInParent<Grabable>())
        {
            grabTracker.UpdateGrabFinger(other.GetComponentInParent<Grabable>(), false, thumb);
        }
    }


}
